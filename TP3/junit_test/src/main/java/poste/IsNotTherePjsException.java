package poste;

public class IsNotTherePjsException extends RuntimeException {
	
	public IsNotTherePjsException(String message) {
		super(message);
	}
}
