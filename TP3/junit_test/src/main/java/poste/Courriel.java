package poste;

import java.util.ArrayList;
public class Courriel{
	
	private String dest;
	private String title;
	private String corps;
	private ArrayList<String> piece;
	
	public Courriel(String dest, String titre, String corps, ArrayList<String> piece) {
		this.dest=dest;
		this.title=titre;
		this.corps=corps;
		this.piece=piece;
	}
	
	public boolean IsMailValid() {
        return dest.matches("^[a-z][a-z0-9.]*@[a-z0-9]+[.][a-z]+$");
    }
	
	public boolean CanbeSend() {
		return !(title.isBlank());
	}
	
	public void Pjs(){
		if(corps.contains("piece jointe") && piece.isEmpty()) {
			throw new IsNotTherePjsException("Une pièce jointe est requise");
		}
	}
	
	public String getCorps() {
		return this.corps;
	}
	
}