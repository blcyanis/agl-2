package fds.um.junit_test;

import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import poste.Courriel;
import poste.IsNotTherePjsException;

public class CourrielTest{
	
	ArrayList<String> corps1=new ArrayList<String>();
	ArrayList<String> corps2=new ArrayList<String>();
	Courriel email1=new Courriel("blcyanis@gmail.com","Demande","vous trouverez",corps1);
	Courriel email2=new Courriel("blcyanis","Demande", "vous trouverez en piece jointe",corps2);
	
	@Test
	public void TestMail() {
		assertTrue(email1.IsMailValid());
		assertFalse(email2.IsMailValid());
	}
	
	@Test
	public void TestTitre() {
		assertTrue(email1.CanbeSend());
		assertTrue(email2.CanbeSend());
	}
	
	@Test 
	public void TestPjs() {
		assertThrows(IsNotTherePjsException.class, ()-> {
			email2.Pjs();
		});
	}
}